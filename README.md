# Othman Abdulsalam Neovim Configuration repo

This repo holds my personal configuration for the Neovim text editor.


This can be downloaded and run to immediately configure a new system's neovim installation to match my preferences.

Many Youtubers were referenced constantly throughout the development of this config, some include:

- ThePrimeagen
- Josean

This repo also hosts my zellij configuration. In order for zellij to read the config.kdl file you must have the following line
inside the profile file for your terminal (bashprofile, zshprofile etc) or wherever environment paths are exported

```shell
export ZELLIJ_CONFIG_DIR=~/.config/nvim
```

For zellij to use neovim as the editor for the Ctrl-s + e scrollback option, add neovim as the default editor like

```shell
export EDITOR='~/.local/share/bob/nvim-bin/nvim'
```

You may have to change the path depending on where the nvim exectuable is installed


Sometimes there may be a need to delete Lazy itself to reinstall it, this command 
will remove lazy and everything installed in the nvim folder. Then simply rerunning 
neovim will reinstall the latest stable release of Lazy


```shell
rm -rf ~/.local/share/nvim/*
```

## Requirements

1. neovim version 0.10 or above (recommend installing using bob neovim version manager)
1. Must have gcc compiler on system for Treesitter to work
2. Lazy needs to be installed although this is taken care of when initialising the project
4. Assumes a QWERTY keyboard layout
5. Must have ripgrep installed on system which can be found [here](https://github.com/BurntSushi/ripgrep)


## Basic Walkthrough

the init.lua file at the root of the repo directory is where the core & lazy files are loaded

core files are for neovim configuration of keymaps in keymap.lua and option modifications (line numbers, tab length etc) in options.lua

lazy files contain all the neovim plugins that get installed via lazy, configuring how they get loaded
and further customising the plugins as desired.

## Issues

A list of some issues that are currently existing in this project that could do with resolving

## Future changes

1. Will change from lsp zero to a more customised config based on Josean's solution so i have more fine control
over the lsp server configs

2. Work on integrating VimLatex plugin to this config to be able to write Latex documents in future
