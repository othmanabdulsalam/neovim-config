return {
    {
        "folke/trouble.nvim",
        icons = {
          indent = {
            middle = " ",
            last = " ",
            top = " ",
            ws = "│  ",
          },
        },
        modes = {
          diagnostics = {
            groups = {
              { "filename", format = "{basename:Title} {count}" },
            },
          },
        },
        config = function()
            require("trouble").setup({
                -- icons = false,
            })
            vim.keymap.set("n", "<leader>tt", "<cmd>Trouble<cr>" )
            vim.keymap.set("n", "<leader>td", "<cmd>Trouble diagnostics toggle<cr>" )
            vim.keymap.set("n", "[t", function()
                require("trouble").next({skip_groups = true, jump = true});
            end)

            vim.keymap.set("n", "]t", function()
                require("trouble").previous({skip_groups = true, jump = true});
            end)

        end
    },
}
