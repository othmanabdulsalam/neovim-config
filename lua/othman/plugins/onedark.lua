return {
        {'navarasu/onedark.nvim',
        priority = 1000,
        config = function()
        local onedark = require('onedark')
        onedark.setup{
          code_style = 
          {
            comments = 'bold'
          },
          colors = {
            comment = "#ffffff",
          },
          highlights = {
            ["@comment"] = { fg = '$comment' }
          }
        }
        onedark.load()
        end,
        },
}
