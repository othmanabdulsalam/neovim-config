local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({ { import = "othman.plugins" }, { import = "othman.plugins.lsp" } }, {
  -- install = {
  --   colorscheme = { "onedark" },
  -- },
  checker = {
    enabled = true,
    notify = false,
  },
  change_detection = {
    notify = false,
  },
  ui = {
    icons = {
      cmd = "CMD: ",
      config = "CONFIG:",
      event = "EVENT:",
      ft = "FILE:",
      init = "INIT:",
      import = "IMPORT:",
      keys = "KEYS",
      lazy = "LAZY:",
      loaded = "YES:",
      not_loaded = "NO:",
      plugin = "PLUGIN:",
      runtime = "RUNTIME:",
      require = "REQUIRE:",
      source = "SOURCE:",
      start = "START:",
      task = "TASK:",
      list = {
        "●",
        "➜",
        "★",
        "‒",
      },
    }
  },
})


-- lazy rebinding
vim.keymap.set("n", "<leader>lz", ":Lazy<CR>" )
