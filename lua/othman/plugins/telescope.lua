return {
  "nvim-telescope/telescope.nvim",
  branch = "0.1.x",
  dependencies = {
    "nvim-lua/plenary.nvim"
  },
  config = function()
    local builtin = require('telescope.builtin')
        vim.keymap.set('n', '<leader>pf', builtin.find_files, {}) -- project files
        vim.keymap.set('n', '<C-p>', builtin.git_files, {}) -- only git tracked files
        vim.keymap.set('n', '<leader>ps', function() -- grep searching files 
        builtin.grep_string({ search = vim.fn.input("Grep > ") })
    end)
  end,
}

